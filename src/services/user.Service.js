import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export let userService = {
  postLogin: (loginData) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },

  postRegister: (registerData) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", registerData);
  },

  layThongTinNguoiDung: () => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  layDanhSachNguoiDung: (danhSach) => { 
    if (danhSach == "") {
      return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP05");
    } else {
      return https.get(
        `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP05&tuKhoa=${danhSach}`
      );
    }
  },

  deleteUser: (taiKhoan) => {
    return axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,
      method: "DELETE",
      data: taiKhoan,
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  layThongTinNguoiDungCapNhat: (taiKhoan) => {
    return axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/LayThongTinNguoiDung?taiKhoan=${taiKhoan}`,
      method: "POST",
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  capNhatThongTinNguoiDung: (values) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
      method: "POST",
      data: values,
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  themNguoiDungVaoDanhSach: (user) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung",
      method: "POST",
      data: user,
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  // timKiemNguoiDung: (values) => {
  //   return https.get(
  //     `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP05&tuKhoa=${values}`
  //   );
  // },
};
