import { https, TOKEN_CYBER } from "./configURL";
import { thongTinDatVe } from "../models/thongTinDatVe";
import axios from "axios";
import { localStorageServ } from "./localStorageService";

export let QuanLyDatVeService = {
  layChiTietPhongVe: (maLichChieu) => {
    return https.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  },

  // layThongTinDatVeService: (layThongTinDatVe = new thongTinDatVe()) => {
  //   return https.post("/api/QuanLyDatVe/DatVe", layThongTinDatVe);
  // },

  // LÀM THEO CÁCH Ở TRÊN BỊ BUG

  layThongTinDatVeService: (layThongTinDatVe = new thongTinDatVe()) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
      method: "POST",
      data: layThongTinDatVe,
      headers: {
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};
