import axios from "axios";
import { store } from "..";
import {
  batLoadingAction,
  tatLoadingAction,
} from "../redux/actions/spinnerAction";

export const TOKEN_CYBER =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJOb2RlanMgMjQiLCJIZXRIYW5TdHJpbmciOiIwOC8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODA5MTIwMDAwMDAiLCJuYmYiOjE2NjQyMTE2MDAsImV4cCI6MTY4MTA1OTYwMH0.HQfkAVQ_C4qH8T3vLwdtQfqQMRnagp260JXmrMN5lVs";

export let https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_CYBER,
  },
});

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    store.dispatch(batLoadingAction());
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    store.dispatch(tatLoadingAction());
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    store.dispatch(tatLoadingAction());

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
