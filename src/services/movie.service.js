import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export let movieService = {
  getMovieList: (tenPhim = "") => {
    if (tenPhim !== "") {
      return https.get(
        `/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05&&tenPhim=${tenPhim}`
      );
    } else {
      return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05");
    }
  },

  // getMovieDetail: (id) => {
  //   return https.get(
  //     `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`
  //   );
  // },

  getMovieByTheater: () => {
    return https.get(
      `https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05`
    );
  },

  getBannerImg: () => {
    return https.get(
      `http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachBanner`
    );
  },

  layThongTinLichChieuPhim: (id) => {
    return https.get(
      `https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`
    );
  },

  themPhimUpLoadHinh: (formData) => {
    return https.post(`/api/QuanLyPhim/ThemPhimUpLoadHinh`, formData);
  },

  layThongTinPhim: (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },

  capNhatPhimUpLoad: (formData) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/CapNhatPhimUpload",
      method: "POST",
      data: formData,
      headers: {
        ["Authorization"]: "bearer " + localStorageServ.user?.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },

  xoaPhim: (maPhim) => {
    return axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
      method: "DELETE",
      headers: {
        ["Authorization"]: "bearer " + localStorageServ.user?.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};
