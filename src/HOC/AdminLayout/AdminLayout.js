import React, { useState } from "react";
import { DesktopOutlined, FileOutlined, UserOutlined } from "@ant-design/icons";
import { Layout, Menu, message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import SubMenu from "antd/lib/menu/SubMenu";
import { localStorageServ } from "../../services/localStorageService";

const { Header, Content, Footer, Sider } = Layout;

export default function AdminLayout({ Component }) {
  const [collapsed, setCollapsed] = useState(false);

  const history = useNavigate();

  let isAdmin =
    localStorageServ.user.get()?.maLoaiNguoiDung === "QuanTri" ? true : false;

  if (isAdmin == true) {
    return (
      <Layout
        style={{
          minHeight: "100vh",
        }}
      >
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <div className="logo">
            <img
              width="150"
              height="50"
              src="https://cybersoft.edu.vn/wp-content/uploads/2021/03/logo-cyber-nav.svg"
              alt=""
            ></img>
          </div>
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item key="1" icon={<UserOutlined />}>
              <NavLink to="/admin/users">Users</NavLink>
            </Menu.Item>

            <SubMenu key="sub1" icon={<FileOutlined />} title="Films">
              <Menu.Item key="10" icon={<FileOutlined />}>
                <NavLink to="/admin/films">Films</NavLink>
              </Menu.Item>

              <Menu.Item key="11" icon={<FileOutlined />}>
                <NavLink to="/admin/films/addnew">Add New</NavLink>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>

        <Layout className="site-layout">
          {/* <Header
            className="site-layout-background"
            style={{
              padding: 0,
              backgroundColor: "white",
            }}
          >
            <h3 className="text-4xl">Header</h3>
          </Header> */}

          <Content
            style={{
              margin: "0 16px",
            }}
          >
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              <Component />
            </div>
          </Content>

          <Footer
            style={{
              textAlign: "center",
            }}
          >
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  } else {
    message.warning("VUI LÒNG ĐĂNG NHẬP TÀI KHOẢN ADMIN");

    setTimeout(() => {
      window.location.href = "/login";
    }, 1500);
  }
}
