import React from "react";
import Footer from "../Page/HomePage/Footer/Footer";
import HeaderTheme from "../component/HeaderTheme/HeaderTheme";

export default function Layout({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
      <Footer />
    </div>
  );
}
