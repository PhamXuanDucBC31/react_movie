import { message, Progress, Rate, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { movieService } from "../../services/movie.service";
import { CustomCard } from "@tsamantanis/react-glassmorphism";
import "@tsamantanis/react-glassmorphism";
import moment from "moment";
import { localStorageServ } from "../../services/localStorageService";
const { TabPane } = Tabs;

export default function DetailPage() {
  let { id } = useParams();

  let history = useNavigate();

  const [movie, setMovie] = useState({});

  useEffect(() => {
    movieService
      .layThongTinLichChieuPhim(id)
      .then((res) => {
        setMovie(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const checkLogin = (id) => {
    if (localStorageServ.user.get() == null) {
      message.warn("VUI LÒNG ĐĂNG NHẬP TRƯỚC KHI ĐẶT VÉ !!!!");
      setTimeout(() => {
        // chuyển trang
        history("/login");
      }, 1000);
    } else {
      history(`/booking/${id}`);
    }
  };

  return (
    <div
      style={{
        backgroundImage: `url(${movie.hinhAnh})`,
        minHeight: "100%",
        backgroundSize: "100%",
        backgroundPosition: "center",
      }}
    >
      <CustomCard
        style={{ paddingTop: "150px", minHeight: "100vh" }}
        effectColor="#e0e7ea" // required
        color="#14AEFF" // default color is white
        blur={10} // default blur value is 10px
        borderRadius={"0px"} // default border radius value is 10px
      >
        <div className="grid grid-cols-12 gap-18">
          <div className="col-span-6 col-start-2">
            <div className="grid grid-cols-6 gap-10 leading-7">
              <img
                className="col-span-3 w-full h-full"
                src={movie.hinhAnh}
                alt=""
              />
              <div className="col-span-3 mt-5">
                <p className="text-sm text-white">
                  Ngày chiếu:{moment(movie.ngayKhoiChieu).format("DD.MM.YYYY")}
                </p>
                <p className="text-4xl font-medium text-white">
                  {movie.biDanh}
                </p>
                <p className="mt-5" style={{ color: "#e9e9e9" }}>
                  {movie.moTa?.replace(/(<([^>]+)>)/gi, "")}
                </p>
              </div>
            </div>
          </div>
          <div className="col-span-4">
            <p
              className="text-green-400 mb-2 text-xl "
              style={{ marginLeft: "4%" }}
            >
              Đánh giá
            </p>
            <div>
              <Rate value={movie.danhGia} />
            </div>
            <Progress
              className="space-x-10 mt-5"
              type="circle"
              percent={movie.danhGia * 10}
              format={(number) => {
                return (
                  <span className=" font-medium" style={{ color: "#e9e9e9" }}>
                    {number / 10} điểm
                  </span>
                );
              }}
            />
          </div>
        </div>

        {/* -----Load thong tin rap va lich chieu theo phim--------- */}

        <div>
          <div className="mt-10 ml-72 w-2/3 bg-white p-5 container">
            <Tabs defaultActiveKey="1" centered="true">
              <Tabs.TabPane tab="Lịch chiếu" key="1" style={{ minHeight: 200 }}>
                <Tabs tabPosition="left">
                  {movie.heThongRapChieu?.map((htr, index) => {
                    return (
                      <TabPane
                        tab={
                          <div>
                            <img
                              src={htr.logo}
                              width={50}
                              height={50}
                              alt=""
                              className="rounded-full"
                            />
                            <div className="mx-auto">{htr.tenHeThongRap}</div>
                          </div>
                        }
                        key={index}
                      >
                        {htr.cumRapChieu?.map((cumRap, index) => {
                          return (
                            <div className="mt-5" key={index}>
                              <div className="flex flex-row">
                                <img
                                  style={{ width: 60, height: 60 }}
                                  src={`https://s3img.vcdn.vn/123phim/2021/01/bhd-star-bitexco-16105952137769.png`}
                                />
                                <div className="ml-2 ">
                                  <p
                                    style={{ fontSize: 20, fontWeigt: "bold" }}
                                  >
                                    {cumRap.tenCumRap}
                                  </p>
                                  <p
                                    className="text-gray-500"
                                    style={{ marginTop: 0 }}
                                  >
                                    {cumRap.tenCumRap}
                                  </p>
                                </div>
                              </div>

                              {/* ---Load NGAY CHIEU GIO CHIEU PHIM --- */}

                              <div className="thong-tin-lich-chieu mt-4 grid grid-cols-4">
                                {cumRap.lichChieuPhim
                                  ?.filter((item, index) => index < 8)
                                  .map((lichChieu, index) => {
                                    return (
                                      <div
                                        key={index}
                                        className="col-span-1 gap-2 bg-red-500 rounded p-1 m-1 text-white font-bold cursor-pointer"
                                        onClick={() => {
                                          checkLogin(lichChieu.maLichChieu);
                                        }}
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("DD-MM-YYYY")}
                                        <span className="text-yellow-200 font-bold text-base ml-5 ">
                                          {moment(
                                            lichChieu.ngayChieuGioChieu
                                          ).format("hh:mm A")}
                                        </span>
                                      </div>
                                    );
                                  })}
                              </div>
                            </div>
                          );
                        })}
                      </TabPane>
                    );
                  })}
                </Tabs>
              </Tabs.TabPane>

              <Tabs.TabPane tab="Thông tin" key="2" style={{ minHeight: 200 }}>
                Thông tin
              </Tabs.TabPane>

              <Tabs.TabPane tab="Đánh giá" key="3" style={{ minHeight: 200 }}>
                Đánh giá
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </CustomCard>
    </div>
  );
}
