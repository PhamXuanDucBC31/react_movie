import React from "react";
import Lottie from "lottie-react";
import bgAnimate from "../../assets/loginAnimate.json";
export default function LoginAnimate() {
  return (
    <div className="transform -translate-y-32">
      <Lottie animationData={bgAnimate} height="200" />
    </div>
  );
}
