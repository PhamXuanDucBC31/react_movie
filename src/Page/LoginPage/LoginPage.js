import React from "react";
import { Form, Input, message } from "antd";
import { userService } from "../../services/user.Service";
import { localStorageServ } from "../../services/localStorageService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import LoginAnimate from "./LoginAnimate";
export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();

  const onFinish = (values) => {
    // history(-1);
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch(loginAction(res.data.content));
        localStorageServ.user.set(res.data.content);
        // window.location.href = "/";
        setTimeout(() => {
          // chuyển trang
          history("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-red-500 w-screen h-screen p-10">
      <div className="container rounded-xl mx-auto p-10 bg-white flex ">
        <div className="w-1/2 h-96 ">
          <LoginAnimate />
        </div>
        <div className="w-1/2">
          <Form
            name="basic"
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label={<p className="text-blue-500 font-medium">Tài khoản</p>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label={<p className="text-blue-500 font-medium">Mật khẩu</p>}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <div className="flex justify-center">
              <button className="rounded px-5 py-2 text-white bg-red-500">
                Đăng nhập {""}
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
