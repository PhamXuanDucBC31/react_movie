import Item from "antd/lib/list/Item";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { layThongTinNguoiDungAction } from "../../redux/actions/userAction";

export default function BookingHistory() {
  const dispatch = useDispatch();

  const { thongTinNguoiDung } = useSelector((state) => state.userReducer);

  const { thongTinDatVe } = thongTinNguoiDung;

  useEffect(() => {
    let action = layThongTinNguoiDungAction();
    dispatch(action);
  }, []);

  const renderLichSuDatVe = () => {
    return thongTinDatVe?.map((danhSachGhe, index) => {
      console.log(danhSachGhe.danhSachGhe);
      return (
        <div key={index} className="text-left bg-green-400 rounded-lg p-10">
          <p className="text-center">Mã vé: {danhSachGhe.maVe}</p>
          <p>
            Ngày đặt :{" "}
            <span>
              {moment(danhSachGhe.ngayDat).format("DD-MM-YYYY  |  hh:mm")}
            </span>
          </p>
          <h1>Tên phim: {danhSachGhe.tenPhim} </h1>
          <p>
            Thời lượng: {danhSachGhe.thoiLuongPhim}, Giá vé: {danhSachGhe.giaVe}
          </p>
          <h2>{danhSachGhe.danhSachGhe.find(() => true).tenHeThongRap}</h2>
          <h3>
            {danhSachGhe.danhSachGhe.find(() => true).tenCumRap}, Ghế số:
            {danhSachGhe.danhSachGhe.map((Item, index) => {
              return <span key={index}> [ {Item.tenGhe} ] </span>;
            })}
          </h3>
        </div>
      );
    });
  };

  return (
    <div className="p-5 container">
      <h3 className="mb-14 text-4xl font-medium">Kết quả đặt vé</h3>
      <div className="grid grid-cols-3 gap-14 text-2xl">
        {renderLichSuDatVe()}
      </div>
    </div>
  );
}
