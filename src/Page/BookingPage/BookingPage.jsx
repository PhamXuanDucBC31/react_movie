import React, { Fragment, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { localStorageServ } from "../../services/localStorageService";
import { Tabs } from "antd";
import {
  layChiTietPhongVeAction,
  layThongTinDatVeAction,
} from "../../redux/actions/QuanLyDatVeAction";
import style from "../../component/BookingPageCSS/BookingPage.module.css";
import { CloseOutlined } from "@ant-design/icons";
import { DAT_VE } from "../../redux/constant/QuanLyDatVeConstant";
import { thongTinDatVe } from "../../models/thongTinDatVe";
import BookingHistory from "../BookingHistory/BookingHistory";

export default function BookingPage(props) {
  let { id } = useParams();
  let dispatch = useDispatch();

  let userLogin = localStorageServ.user.get();

  const { chiTietPhongVe, danhSachGheDangDat } = useSelector(
    (state) => state.QuanLyDatVeReducer
  );

  useEffect(() => {
    dispatch(layChiTietPhongVeAction(id));
  }, []);

  const { thongTinPhim, danhSachGhe } = chiTietPhongVe;

  const renderGhe = () => {
    return danhSachGhe?.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";

      let classDaDat = ghe.daDat === true ? "gheDaDat" : "";

      let classDangDat = "";

      let indexGheDangDat = danhSachGheDangDat.findIndex((gheDangDAt) => {
        return gheDangDAt.maGhe === ghe.maGhe;
      });

      if (indexGheDangDat != -1) {
        classDangDat = "gheDangDat";
      }

      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              dispatch({
                type: DAT_VE,
                gheDuocChon: ghe,
              });
            }}
            disabled={ghe.daDat}
            className={`${style["ghe"]} ${style[classGheVip]} ${style[classDaDat]} ${style[classDangDat]}`}
          >
            {/* render ghế */}
            {ghe.daDat ? (
              <CloseOutlined
                style={{
                  fontSize: "20px",
                }}
              />
            ) : (
              ghe.stt
            )}
          </button>

          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };

  const renderGheDangDat = () => {
    return danhSachGheDangDat.map((gheDangDat, index) => {
      return (
        <span key={index} className="ml-1 font-medium text-green-500">
          {gheDangDat.stt}
        </span>
      );
    });
  };

  const tinhTien = () => {
    return danhSachGheDangDat
      .reduce((tongTien, ghe, index) => {
        return (tongTien += ghe.giaVe);
      }, 0)
      .toLocaleString();
  };

  const datVe = () => {
    const thongTinVe = new thongTinDatVe();
    thongTinVe.maLichChieu = id;

    thongTinVe.danhSachVe = danhSachGheDangDat;

    dispatch(layThongTinDatVeAction(thongTinVe));
  };

  return (
    <div className="container h-screen px-10 pt-5">
      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab="1. CHỌN GHẾ VÀ THANH TOÁN" key="1">
          <div className="flex flex-row">
            <div className="basis-3/4">
              <div className="w-full h-16 text-white flex justify-center items-center text-2xl font-medium bg-gray-500">
                <span>Màn hình</span>
              </div>
              <div>{renderGhe()}</div>
            </div>

            <div className="basis-1/4 h-screen pt-20">
              <h3 className="text-green-400 text-center text-2xl">
                {tinhTien()} VNĐ
              </h3>

              <hr className="my-3" />

              <h3 className="text-4xl my-3 font-bold ">
                {thongTinPhim?.tenPhim}
              </h3>

              <p className="text-left font-medium">{thongTinPhim?.tenCumRap}</p>

              <p className="text-left text-red-800">{thongTinPhim?.diaChi}</p>

              <hr className="my-3" />

              <p className="text-left my-1 flex flex-row">
                Suất chiếu:{" "}
                <div className="ml-1 font-medium">
                  {" "}
                  {thongTinPhim?.ngayChieu} - {thongTinPhim?.gioChieu}
                </div>
              </p>

              <p className="text-left my-1 flex flex-row">
                Rạp:{" "}
                <div className="ml-1 font-medium"> {thongTinPhim?.tenRap}</div>
              </p>

              <hr className="my-3" />

              <div className="text-left my-3 flex flex-row">
                <span className="mr-1">Ghế:</span>
                <div className="grid grid-cols-10 gap-2">
                  {renderGheDangDat()}
                </div>
              </div>

              <hr />

              <div className=" text-left my-3">
                <i>E-mail:</i> <br />
                <p>{userLogin.email}</p>
              </div>

              <div className=" text-left my-3">
                <i>Phone:</i> <br />
                <p>{userLogin.soDT}</p>
              </div>

              <div className="mt-20 flex flex-col justify-end">
                <button
                  onClick={() => {
                    datVe();
                  }}
                  className="bg-green-500 hover:bg-green-800 text-white font-bold py-2 px-4 rounded"
                >
                  Đặt vé
                </button>
              </div>
            </div>
          </div>
        </Tabs.TabPane>

        <Tabs.TabPane tab="2. LỊCH SỬ ĐẶT VÉ" key="2">
          <BookingHistory />
        </Tabs.TabPane>
      </Tabs>
    </div>
  );
}
