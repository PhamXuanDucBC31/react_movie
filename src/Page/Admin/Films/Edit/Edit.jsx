import {
  Form,
  Radio,
  Input,
  DatePicker,
  Switch,
  InputNumber,
  Button,
} from "antd";
import { useFormik } from "formik";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import "./edit.css";
import {
  capNhatPhimUpLoadAction,
  layThongTinPhimAction,
  themPhimUploadHinhAction,
} from "../../../../redux/actions/movieAction";
import { localStorageServ } from "../../../../services/localStorageService";

export default function Edit() {
  const [componentSize, setComponentSize] = useState("default");
  const [imgSrc, setImgSrc] = useState("");
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    dispatch(layThongTinPhimAction(id));
  }, []);

  const { thongTinPhim } = useSelector((state) => state.movieReducer);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      maPhim: thongTinPhim.maPhim,
      tenPhim: thongTinPhim.tenPhim,
      trailer: thongTinPhim.trailer,
      moTa: thongTinPhim.moTa,
      ngayKhoiChieu: thongTinPhim.ngayKhoiChieu,
      dangChieu: thongTinPhim?.dangChieu,
      sapChieu: thongTinPhim?.sapChieu,
      hot: thongTinPhim?.hot,
      danhGia: thongTinPhim.danhGia,
      hinhAnh: null,
    },
    onSubmit: (values) => {
      // Tạo đối tượng formData
      values.maNhom = "GP05";
      let formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          if (values.hinhAnh != null) {
            formData.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }

      //dispatch formDAta lên redux

      dispatch(capNhatPhimUpLoadAction(formData));
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeDatePicker = (value) => {
    let ngayKhoiChieu = moment(value);
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  const handleChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeFile = async (e) => {
    // Lấy file từ e
    let file = e.target.files[0];
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png" ||
      file.type === "image/gif"
    ) {
      // Tạo đối tượng để đọc file

      await formik.setFieldValue("hinhAnh", file);
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        setImgSrc(e.target.result); // Hình base 64
      };
      // Đem dữ liệu file đưa vào fomik
    }
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Tên phim" onChange={formik.handleChange}>
        <Input value={formik.values.tenPhim} name="tenPhim" />
      </Form.Item>

      <Form.Item label="Trailer" onChange={formik.handleChange}>
        <Input value={formik.values.trailer} name="trailer" />
      </Form.Item>

      <Form.Item label="Mô tả" onChange={formik.handleChange}>
        <Input value={formik.values.moTa} name="moTa" />
      </Form.Item>

      <Form.Item label="Ngày khởi chiếu">
        <DatePicker
          format={"DD/MM/YYYY"}
          onChange={handleChangeDatePicker}
          value={moment(formik.values.ngayKhoiChieu)}
        />
      </Form.Item>

      <Form.Item label="Đang chiếu">
        <Switch
          checked={formik.values.dangChieu}
          onChange={handleChangeSwitch("dangChieu")}
        />
      </Form.Item>

      <Form.Item label="Sắp chiếu">
        <Switch
          checked={formik.values.sapChieu}
          onChange={handleChangeSwitch("sapChieu")}
        />
      </Form.Item>

      <Form.Item label="Hot">
        <Switch
          checked={formik.values.hot}
          onChange={handleChangeSwitch("hot")}
        />
      </Form.Item>

      <Form.Item label="Số sao">
        <InputNumber
          value={formik.values.danhGia}
          onChange={handleChangeInputNumber("danhGia")}
          min={0}
          max={10}
        />
      </Form.Item>

      <Form.Item label="Hình ảnh">
        <input
          type="file"
          onChange={handleChangeFile}
          accept="image/png, image/jpeg , image/gif , image/png"
        />
        <br />
        <img
          src={imgSrc === "" ? thongTinPhim.hinhAnh : imgSrc}
          style={{ height: 150, width: 150 }}
          alt="..."
        />
      </Form.Item>

      <Form.Item label="Tác vụ">
        <button
          type="submit"
          className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
        >
          CẬP NHẬT PHIM
        </button>
      </Form.Item>
    </Form>
  );
}
