import { DatePicker, Form, Input, InputNumber, Radio, Switch } from "antd";
import React, { useState } from "react";
import "./AddNew.css";
import { useFormik } from "formik";
import moment from "moment";
import { themPhimUploadHinhAction } from "../../../../redux/actions/movieAction";
import { useDispatch } from "react-redux";

export default function AddNew() {
  const [componentSize, setComponentSize] = useState("default");
  const [imgSrc, setImgSrc] = useState(null);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      mota: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      hinhAnh: {},
    },
    onSubmit: (values) => {
      console.log("values: ", values);
      // Tạo đối tượng formData
      values.maNhom = "GP05";
      let formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else formData.append("File", values.hinhAnh, values.hinhAnh.name);
      }
      //   // Gọi api gửi các giá trị form data về backend xử lý
      dispatch(themPhimUploadHinhAction(formData));
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeDatePicker = (value) => {
    let ngayKhoiChieu = moment(value).format("DD/MM/YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  const handleChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeFile = (e) => {
    // Lấy file từ e
    let file = e.target.files[0];
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png" ||
      file.type === "image/gif"
    ) {
      // Tạo đối tượng để đọc file
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        setImgSrc(e.target.result); // Hình base 64
      };
      // Đem dữ liệu file đưa vào fomik
      formik.setFieldValue("hinhAnh", file);
    }
  };
  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Thêm mới phim</h3>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="Tên phim" onChange={formik.handleChange}>
          <Input name="tenPhim" />
        </Form.Item>

        <Form.Item label="Trailer" onChange={formik.handleChange}>
          <Input name="trailer" />
        </Form.Item>

        <Form.Item label="Mô tả" onChange={formik.handleChange}>
          <Input name="moTa" />
        </Form.Item>

        <Form.Item label="Ngày khởi chiếu">
          <DatePicker format={"DD/MM/YYYY"} onChange={handleChangeDatePicker} />
        </Form.Item>

        <Form.Item label="Đang chiếu">
          <Switch onChange={handleChangeSwitch("dangChieu")} />
        </Form.Item>

        <Form.Item label="Sắp chiếu">
          <Switch onChange={handleChangeSwitch("sapChieu")} />
        </Form.Item>

        <Form.Item label="Hot">
          <Switch onChange={handleChangeSwitch("hot")} />
        </Form.Item>

        <Form.Item label="Số sao">
          <InputNumber
            onChange={handleChangeInputNumber("danhGia")}
            min={0}
            max={10}
          />
        </Form.Item>

        <Form.Item label="Hình ảnh">
          <input
            type="file"
            onChange={handleChangeFile}
            accept="image/png, image/jpeg , image/gif , image/png"
          />
          <br />
          <img src={imgSrc} style={{ height: 150, width: 150 }} alt="..." />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button type="submit" className="add_film text-white p-2">
            Thêm phim
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
