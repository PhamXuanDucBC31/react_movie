import { Form, Input, Radio, Select } from "antd";
import React, { useState } from "react";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { formatCountdown } from "antd/lib/statistic/utils";
import { themNguoiDungVaoFormAction } from "../../../../redux/actions/userAction";

export default function AddNew() {
  const [componentSize, setComponentSize] = useState("default");
  const dispatch = useDispatch();
  const [select, setSelect] = useState("");

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: "",
      maLoaiNguoiDung: "",
      hoTen: "",
    },
    onSubmit: (values) => {
      //   // Gọi api gửi các giá trị form data về backend xử lý
      dispatch(themNguoiDungVaoFormAction(values));
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeSelect = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Thêm người dùng</h3>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="Tài khoản" onChange={formik.handleChange}>
          <Input name="taiKhoan" />
        </Form.Item>

        <Form.Item label="Mật khẩu" onChange={formik.handleChange}>
          <Input name="matKhau" />
        </Form.Item>

        <Form.Item label="E-mail" onChange={formik.handleChange}>
          <Input name="email" />
        </Form.Item>

        <Form.Item label="Số điện thoại" onChange={formik.handleChange}>
          <Input name="soDt" />
        </Form.Item>

        <Form.Item label="Mã nhóm" onChange={formik.handleChange}>
          <Input name="maNhom" />
        </Form.Item>

        <Form.Item label="Loại người dùng">
          <Select onChange={handleChangeSelect("maLoaiNguoiDung")}>
            <Select.Option value="QuanTri">QuanTri</Select.Option>
            <Select.Option value="KhachHang">KhachHang</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Họ tên" onChange={formik.handleChange}>
          <Input name="hoTen" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            THÊM NGƯỜI DÙNG
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
