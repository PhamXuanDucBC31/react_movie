import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteUserAction,
  layDanhSachNguoiDungAction,
} from "../../../redux/actions/userAction";
import {
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { Table, Input } from "antd";

export default function DashBoard() {
  const dispatch = useDispatch();

  const { danhSachNguoiDung } = useSelector((state) => state.userReducer);
  const { timKiemNguoiDung } = useSelector((state) => state.userReducer);

  const data = danhSachNguoiDung;

  const { Search } = Input;

  const onSearch = (value) => {
    dispatch(layDanhSachNguoiDungAction(value));
  };

  const deleteUser = (taiKhoan) => {
    dispatch(deleteUserAction(taiKhoan));
  };

  const columns = [
    // render tài khoản
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let taiKhoanA = a.taiKhoan.toLowerCase().trim();
        let taiKhoanB = b.taiKhoan.toLowerCase().trim();
        if (taiKhoanA > taiKhoanB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    // render họ tên
    {
      title: "Họ tên",
      dataIndex: "hoTen",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let hoTenA = a.hoTen.toLowerCase().trim();
        let hoTenB = b.hoTen.toLowerCase().trim();
        if (hoTenA > hoTenB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render email
    {
      title: "E-mail",
      dataIndex: "email",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => {
        let emailA = a.email.toLowerCase().trim();
        let emailB = b.email.toLowerCase().trim();
        if (emailA > emailB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    //rener role người dùng
    {
      title: "Loại người dùng",
      dataIndex: "maLoaiNguoiDung",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sortDirections: ["descend", "ascend"],
    },

    //render thao tác
    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "10%",

      sortDirections: ["descend", "ascend"],
      render: (text, danhSachNguoiDung, index) => {
        return (
          <Fragment>
            <NavLink
              key={1}
              className="mr-2 text-2xl"
              to={`/admin/users/edit/${danhSachNguoiDung.taiKhoan}`}
            >
              <EditOutlined style={{ color: "blue" }} />
            </NavLink>

            <NavLink
              key={2}
              className="ml-2 text-2xl"
              onClick={() => {
                deleteUser(danhSachNguoiDung.taiKhoan);
              }}
            >
              <DeleteOutlined style={{ color: "red" }} />
            </NavLink>
          </Fragment>
        );
      },
    },
  ];

  useEffect(() => {
    dispatch(layDanhSachNguoiDungAction());
  }, []);

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-4xl mb-5">Quản lý người dùng</h3>

      <Search
        className="my-5"
        placeholder="input search text"
        enterButton={<SearchOutlined />}
        size="large"
        onSearch={onSearch}
      />

      <div className="mb-5">
        <button
          onClick={() => {
            window.location.href = "/admin/users/addUser";
          }}
          className=" bg-green-500 font-semibold py-3 rounded-xl hover:bg-white hover:border-2 hover:border-green-500 px-5"
        >
          Thêm người dùng
        </button>
      </div>

      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
