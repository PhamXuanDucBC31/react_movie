import React from "react";
import { Button, Form, Input, message } from "antd";
import "./RegisterPage.css";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/user.Service";
import {
  putThongTinNguoiDungAction,
  registerAction,
} from "../../redux/actions/userAction";
import { localStorageServ } from "../../services/localStorageService";
export default function RegisterPage() {
  let dispatch = useDispatch();
  let history = useNavigate();

  const onFinish = (values) => {
    userService
      .postRegister(values)
      .then((res) => {
        message.success("Đăng ký thành công");

        dispatch(registerAction(res.data.content));

        localStorageServ.user.set(res.data.content);
        setTimeout(() => {
          // chuyển trang
          history("/login");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  /* eslint-disable no-template-curly-in-string */

  const validateMessages = {
    required: "${label} is required!",
  };
  /* eslint-enable no-template-curly-in-string */

  return (
    <div className="register_page">
      <div className="container content py-2 px-5">
        <div className="logo">
          <img src="https://i.imgur.com/lC22izJ.png" alt="" />
        </div>
        <p
          style={{
            textAlign: "center",
            marginBottom: 30,
            marginTop: 10,
            color: "orange",
          }}
        >
          Đăng Ký để được nhiều ưu đãi, mua vé và bảo mật thông tin!
        </p>
        <Form
          {...layout}
          name="nest-messages"
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item
            name="taiKhoan"
            label="Tài khoản"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="matKhau"
            label="Mật khẩu"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="soDt"
            label="Số điện thoại"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="maNhom"
            label="Mã nhóm"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="hoTen"
            label="Họ tên"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
            <Button type="primary" htmlType="submit">
              Đăng ký
            </Button>
          </Form.Item>
        </Form>
        <button
          className="absolute -top-4 -right-4 font-bold text-2xl bg-gray-500 rounded-full px-2"
          onClick={() => {
            window.location.href = "/";
          }}
        >
          X
        </button>
      </div>
    </div>
  );
}
