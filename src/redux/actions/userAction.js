import { message } from "antd";
import { userService } from "../../services/user.Service";
import {
  GET_DANH_SACH_NGUOI_DUNG,
  GET_TIM_KIEM_NGUOI_DUNG,
  POST_LAY_THONG_TIN_NGUOI_DUNG,
  SET_THONG_TIN_NGUOI_DUNG,
} from "../constant/useConstant";

export let loginAction = (dataLogin) => {
  return {
    type: "LOGIN",
    payload: dataLogin,
  };
};

export let registerAction = (dataRegister) => {
  return {
    type: "REGISTER",
    payload: dataRegister,
  };
};

export let layThongTinNguoiDungAction = () => {
  return (dispatch) => {
    userService
      .layThongTinNguoiDung()
      .then((res) => {
        dispatch({
          type: SET_THONG_TIN_NGUOI_DUNG,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export let layDanhSachNguoiDungAction = (danhSach = "") => {
  return (dispatch) => {
    userService
      .layDanhSachNguoiDung(danhSach)
      .then((res) => {
        dispatch({
          type: GET_DANH_SACH_NGUOI_DUNG,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export let deleteUserAction = (taiKhoan) => {
  return (dispatch) => {
    userService
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("xoá thành công");
        setTimeout(() => {
          dispatch(layDanhSachNguoiDungAction());
        }, 500);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
};

export let layThongTinNguoiDungCapNhatAction = (taiKhoan) => {
  return (dispatch) => {
    userService
      .layThongTinNguoiDungCapNhat(taiKhoan)
      .then((res) => {
        dispatch({
          type: POST_LAY_THONG_TIN_NGUOI_DUNG,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export let capNhatThongTinNguoiDungAction = (values) => {
  return (dispatch) => {
    userService
      .capNhatThongTinNguoiDung(values)
      .then((res) => {
        message.success(res.data.message);
        setTimeout(() => {
          window.location.href = "/admin/users";
        }, 1000);
      })
      .catch((err) => {
        message.warn(err.response.data.content);
      });
  };
};

export let themNguoiDungVaoFormAction = (user) => {
  userService
    .themNguoiDungVaoDanhSach(user)
    .then((res) => {
      message.success(res.data.message);
      setTimeout(() => {
        window.location.href = "/admin/users";
      }, 1000);
    })
    .catch((err) => {
      message.warning(err.response.data.content);
    });
};

// export let timKiemNguoiDungAction = (values) => {
//   return (dispatch) => {
//     userService
//       .timKiemNguoiDung(values)
//       .then((res) => {
//         message.success(res.data.message);

//         dispatch({
//           type: GET_TIM_KIEM_NGUOI_DUNG,
//           payload: res.data.content,
//         });
//       })
//       .catch((err) => {
//         message.warning(err.response.data.content);
//       });
//   };
// };
