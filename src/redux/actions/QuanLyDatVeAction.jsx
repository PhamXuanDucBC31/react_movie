import { QuanLyDatVeService } from "../../services/QuanLyDatVeService";
import {
  LAY_THONG_TIN_DAT_VE,
  SET_CHI_TIET_PHONG_VE,
} from "../constant/QuanLyDatVeConstant";

export const layChiTietPhongVeAction = (maLichChieu) => {
  return (dispatch) => {
    QuanLyDatVeService.layChiTietPhongVe(maLichChieu)
      .then((res) => {
        dispatch({
          type: SET_CHI_TIET_PHONG_VE,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const layThongTinDatVeAction = (thongTinDatVe) => {
  return (dispatch) => {
    QuanLyDatVeService.layThongTinDatVeService(thongTinDatVe)
      .then((res) => {
        dispatch({
          type: LAY_THONG_TIN_DAT_VE,
          payload: thongTinDatVe,
        });

        dispatch(layChiTietPhongVeAction(thongTinDatVe.maLichChieu));
      })
      .catch((err) => {});
  };
};
