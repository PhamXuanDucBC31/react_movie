import { message } from "antd";
import {
  DAT_VE,
  LAY_THONG_TIN_DAT_VE,
  SET_CHI_TIET_PHONG_VE,
} from "../constant/QuanLyDatVeConstant";

const initialState = {
  chiTietPhongVe: {},
  danhSachGheDangDat: [],
  thongTinDatVe: [],
};

export let QuanLyDatVeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CHI_TIET_PHONG_VE: {
      state.chiTietPhongVe = action.payload;
      return { ...state };
    }

    case DAT_VE: {
      let danhSachGheCapNhat = [...state.danhSachGheDangDat];

      let index = danhSachGheCapNhat.findIndex((gheDangDat) => {
        return gheDangDat.maGhe === action.gheDuocChon.maGhe;
      });

      if (index != -1) {
        danhSachGheCapNhat.splice(index, 1);
      } else {
        danhSachGheCapNhat.push(action.gheDuocChon);
      }

      return { ...state, danhSachGheDangDat: danhSachGheCapNhat };
    }

    case LAY_THONG_TIN_DAT_VE: {
      message.success("ĐẶT VÉ THÀNH CÔNG");

      return { ...state };
    }
    default:
      return state;
  }
};
