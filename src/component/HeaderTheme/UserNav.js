import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import { localStorageServ } from "../../services/localStorageService";

export default function UserNav() {
  let dispatch = useDispatch();
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  const renderContent = () => {
    if (userInfor) {
      return (
        <div className="space-x-5 items-center flex">
          <p
            onClick={() => {
              window.location.href = "/history";
            }}
            className="text-white capitalize cursor-pointer"
          >
            {userInfor.hoTen}
          </p>
          <button
            onClick={() => {
              handleLogout();
            }}
            className="border-red-500 text-white hover:text-red-500 rounded px-5 py-3 border-2"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="space-x-5 items-center">
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-blue-500 text-white hover:text-blue-500 duration-75 font-bold rounded px-5 py-3 border-2"
          >
            Đăng nhập
          </button>
          <button
            className="border-green-500 text-white hover:text-green-500 duration-75 font-bold rounded px-5 py-3 border-2"
            onClick={() => {
              window.location.href = "/register";
            }}
          >
            Đăng kí
          </button>
        </div>
      );
    }
  };
  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };
  return <div className="flex ">{renderContent()}</div>;
}
